package cz.wudipetr.dumbserver;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.*;
import java.util.Scanner;

/**
 * HTTP handler. Decides what to do with request for page with a input.
 */
public class InputPageHandler implements HttpHandler {
    public void handle(HttpExchange t) throws IOException {
        System.out.println("Yaaaay. A page request. With input. Gonna draw inputs. Lots of them.");
        logInputs(t);
        String response = "<html><head></head><body><form action=\".\" method=\"post\">" +
                "<input type=\"text\" name=\"trubice\"/>" +
                "<input type=\"text\" name=\"hovezivyvar\"/>" +
                "<input type=\"submit\"/>" +
                "</form></body></html>";
        t.sendResponseHeaders(200, response.length());
        OutputStream os = t.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    private void logInputs(HttpExchange t) {
        InputStream inputStream = t.getRequestBody();
        Scanner s = new Scanner(inputStream);
        String result = s.hasNext() ? s.next() : "";
        System.out.println("Pure result: " + result);

        String trubice = "---unknown---";
        String hovezivyvar = "---unknown---";

        // Split it
        String[] params = result.split("&");
        for(String param : params) {
            String[] paramParts = param.split("=");
            if(paramParts.length != 2) {
                continue; // Something weird
            }
            String key = paramParts[0];
            String value = paramParts[1];
            if(key.compareTo("trubice") == 0) {
                trubice = value;
            }
            else if(key.compareTo("hovezivyvar") == 0) {
                hovezivyvar = value;
            }
        }

        System.out.println("Params were:");
        System.out.println("Trubice: " + trubice);
        System.out.println("Hovězí vývar: " + hovezivyvar);
    }
}
