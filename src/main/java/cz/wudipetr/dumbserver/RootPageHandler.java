package cz.wudipetr.dumbserver;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * HTTP handler. Decides what to do with request for home page.
 */
public class RootPageHandler implements HttpHandler {
    public void handle(HttpExchange t) throws IOException {
        System.out.println("Yaaaay. A page request. Thank you, my lord, thank you. Gonna handle it.");
        InputStream is = t.getRequestBody();
        ImageIO.read(is); // .. read the request body
        String response = "<html><head></head><body><img src=\"https://i.imgflip.com/mi1ql.jpg\" alt=\"It works!\"/></body>";
        t.sendResponseHeaders(200, response.length());
        OutputStream os = t.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }
}
