package cz.wudipetr.dumbserver;

import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;


public class Main {

    private static final int SERVER_PORT_DEFAULT = 8081;
    private static final int SERVER_BACKLOG_DEFAULT = 10;

    public static void main(String[] args) {
        try {
            HttpServer server = HttpServer.create(new InetSocketAddress(SERVER_PORT_DEFAULT), SERVER_BACKLOG_DEFAULT);
            server.createContext("/", new RootPageHandler());
            server.createContext("/inputs/", new InputPageHandler());
            server.setExecutor(null); // creates a default executor
            System.out.println("Server is listening on port " + SERVER_PORT_DEFAULT + ".");
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
