# Dumb server
Server that can draw two basic pages and process simple request parameters.

It listens on port 8081 and creates two pages:

* localhost:8081 - or everywhere else: just basic page
* localhost:8081/inputs/ - page with two inputs. They are named "trubka" and
"hovezivyvar" and their content appears in log (standard output) after submit.


## Installation
* Clone or download the project
* `mvn clean package` (requires maven)
* java -jar target/dumb-server-1.0.0.jar
